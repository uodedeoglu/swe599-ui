from django.db import models

class Image(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='images')