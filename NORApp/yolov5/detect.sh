#!/bin/bash

echo $@
echo $cwd
python ./yolov5/detect.py --img 1024 --data ./NORApp/media/data/custom.yaml --source $1 --weight ./NORApp/media/weights/trained-weight.pt --conf-thres 0.5 --project $1