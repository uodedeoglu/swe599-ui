from django.shortcuts import render
from PIL import Image
from .forms import ImageForm

from .yolov5model import predict


def index(request):
    """Process images uploaded by users"""
    
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # Get the current instance object to display in the template
            img_obj = form.instance
            
            predict(img_obj.image.path)
            
            return render(request, 'yolov5index.html', {'form': form, 'img_obj': img_obj})
    else:
        form = ImageForm()
    return render(request, 'yolov5index.html', {'form': form})