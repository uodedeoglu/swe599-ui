# load yolov3 model and perform object detection
# based on https://github.com/experiencor/keras-yolo3
import subprocess
import os
from django.conf import settings
from threading import Thread

def predict(file_name):
	print(file_name)
	thread = Thread(target=call_subprocess, args=[file_name])
	thread.start()
	thread.join() # waits for completion.

 
def call_subprocess(file_name):
	print(os.path.join(settings.BASE_DIR, 'yolov5', 'detect.sh')+f" {file_name}")
	process = subprocess.check_call([os.path.join(settings.BASE_DIR, 'yolov5', 'detect.sh')+f" {file_name}"], shell=True)
